<?php

namespace Xbwsoft\widgets;

use Yii;


/**
 * 文件上传
 * @package common\models
 */
class UploadForm extends \yii\base\Model
{
    public $image;

    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'required', 'on' => ['image']],
            [['file'], 'required', 'on' => ['file']],
            ['image', 'file',
                'skipOnEmpty' => true,
                'extensions'  => 'gif, jpg, png',
                'maxSize'     => 1024*1024*5,
                'tooBig'      => '文件不能超过5M',
                'mimeTypes'   => 'image/gif, image/jpeg, image/png',
            ],
            ['file', 'file',
                'skipOnEmpty' => true,
                'maxSize'     => 1024*1024*5,
                'tooBig'      => '文件不能超过5M',
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['image'] = $scenarios['default'];
        $scenarios['file']  = $scenarios['default'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => '图片文件',
            'file'  => '普通文件',
        ];
    }

}
