<?php

namespace Xbwsoft\widgets;

use Yii;
use yii\base\Component;
use yii\helpers\FileHelper;
use yii\web\UploadedFile as YiiUploadedFile;

/**
 * 文件上传辅助类
 * User: kent173
 * Date: 2018/9/3
 * Time: 16:14
 */

class UploadComponent extends Component
{
    //文件校验MODEL
    public $modelClass;

    //推送保存URL
    public $pathSign;

    public function init()
    {
        parent::init();
        if (empty($this->modelClass)) {
            $this->modelClass = UploadForm::className();
        }
    }

    /**
     * 校验文件
     * @param $attribute
     * @return array
     */
    public function validate($attribute)
    {
        $return = [
            'success'    => false,
            'message'    => [],
        ];
        $file      = YiiUploadedFile::getInstanceByName($attribute);
        $model     = new $this->modelClass;
        $model->setScenario($attribute);
        $model->{$attribute} = $file;
        if (!$model->validate($attribute)) {
            $return['success']  = false;
            $return['message']  = $model->getErrors($attribute);
        } else {
            $return['success']  = true;
        }
        return $return;
    }

    /**
     * 本地上传代码，可重写此方法
     * @return array
     */
    public function imageSaveAs()
    {
        $return = [];
        //校验文件
        $validate = $this->validate(FileInput::TYPE_IMAGE);
        if ($validate['success']) {
            //默认本地上传
            $this->mkdir($this->pathSign);
            $file     = YiiUploadedFile::getInstanceByName(FileInput::TYPE_IMAGE);
            $baseName = md5($file->baseName.rand(100,999));
            $fileName = $this->pathSign . $baseName . '.' . $file->extension;
            if ($file->saveAs($this->pathSign . $baseName . '.' . $file->extension)) {
                $return['success']  = true;
                $return['url']      = Yii::$app->homeUrl . $fileName;
            } else {
                $return['success'] = false;
                $return['message'] = "上传失败";
            }
        } else {
            $return['success'] = true;
            $return['message'] = $validate['message'];
        }
        return $return;
    }

    protected function mkdir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir);
        }
    }

}
