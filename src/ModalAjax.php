<?php
namespace Xbwsoft\widgets;

use yii\bootstrap\Modal;

/**
 * Class ModalAjax.
 * User: kent173
 * Date: 2019/1/24
 * Time: 16:53
 */
class ModalAjax extends Modal {

    /**
     * 触发Modal请求的选择器
     * @var string
     */
    public $selector;

    /**
     * 自动关闭
     * @var bool
     */
    public $autoClose = false;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        parent::run();

        if (empty($this->selector)) {
            return;
        }
        $view = $this->getView();
        $id = $this->getId();
        $js = <<< JS
    $(document).on('click', '{$this->selector}', function () {
        var url = $(this).attr('data-url');
        if (!url) {
            return false;
        }
        $.get(url, function (data) {
            $('#{$id}').find('.modal-body').html(data);
        });
    });
JS;
        $view->registerJs($js);
    }
}